package com.alterovych.pricing.model;

import com.alterovych.pricing.Utils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Date;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Double price;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    @Column(nullable = false)
    private String date;

    public Product() {
    }

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
        setCurrentTime();
    }

    public Product(String name, Double price, String date) {
        this.name = name;
        this.price = price;
        setDateAndTimestamp(date);
    }

    private void setCurrentTime() {
        Instant instant = Instant.now();
        this.date = instant.toString();
        this.timestamp = Date.from(instant.atZone(ZoneOffset.UTC).toInstant());
    }

    public void setDateAndTimestamp(String date) {
        Date timestamp = Utils.parseGmtDate(date);
        if (timestamp == null) {
            setCurrentTime();
        } else {
            this.timestamp = timestamp;
            this.date = Utils.formatDateToGmt(timestamp);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        if (!getId().equals(product.getId())) {
            return false;
        }
        if (!getName().equals(product.getName())) {
            return false;
        }
        if (!getPrice().equals(product.getPrice())) {
            return false;
        }
        return getTimestamp().equals(product.getTimestamp());
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + getName().hashCode();
        result = 31 * result + getPrice().hashCode();
        result = 31 * result + getTimestamp().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", price=" + price +
            ", date=" + date +
            '}';
    }
}
