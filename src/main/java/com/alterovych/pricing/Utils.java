package com.alterovych.pricing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class Utils {
    private final static Logger LOGGER = LoggerFactory.getLogger(Utils.class);
    private final static String DATE_PATTERN = "yyyy-MM-dd";
    private final static String GMT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private final static SimpleDateFormat PARSE_SDF = new SimpleDateFormat(DATE_PATTERN);
    private final static SimpleDateFormat FORMAT_SDF = new SimpleDateFormat(GMT_DATE_FORMAT);

    private Utils() {
    }

    @Nullable
    public static Date parseGmtDate(@Nonnull String stringDate) {
        PARSE_SDF.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            return PARSE_SDF.parse(stringDate);
        }
        catch (ParseException e) {
            LOGGER.error("Wrong date format! '{}' expected!", DATE_PATTERN);
        }
        return null;
    }

    @Nonnull
    public static String formatDateToGmt(@Nonnull Date date) {
        return FORMAT_SDF.format(date);
    }
}
