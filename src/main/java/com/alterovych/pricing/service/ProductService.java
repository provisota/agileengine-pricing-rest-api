package com.alterovych.pricing.service;

import com.alterovych.pricing.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> findAll();

    Product findOne(long id);

    List<Product> findByName(String name);

    List<Product> findByDate(String date);

    Product save(Product product);

    void delete(long id);
}
