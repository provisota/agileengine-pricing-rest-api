package com.alterovych.pricing.service;

import com.alterovych.pricing.Utils;
import com.alterovych.pricing.model.Product;
import com.alterovych.pricing.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl
    implements ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);
    @Autowired
    private ProductRepository repository;

    public List<Product> findAll() {
        return repository.findAll();
    }

    public Product findOne(long id) {
        return repository.findOne(id);
    }

    public List<Product> findByName(String name) {
        return repository.findByName(name);
    }

    public List<Product> findByDate(String date) {
        List<Product> allProductsBeforeDate = repository.findByTimestampLessThanEqual(Utils.parseGmtDate(date));
        return getMostRecentProducts(allProductsBeforeDate);
    }

    private List<Product> getMostRecentProducts(List<Product> allProductsBeforeDate) {
        Map<String, Product> productsMap = new HashMap<String, Product>();
        for (Product product : allProductsBeforeDate) {
            if (productsMap.get(product.getName()) == null) {
                productsMap.put(product.getName(), product);
                continue;
            }
            if (productsMap.get(product.getName()).getTimestamp().before(product.getTimestamp())) {
                productsMap.put(product.getName(), product);
            }
        }
        return new ArrayList<Product>(productsMap.values());
    }

    public Product save(Product product) {
        return repository.save(product);
    }

    public void delete(long id) {
        repository.delete(id);
    }

    @PostConstruct
    public void populateDummyProducts() {
        repository.save(new Product("Bread", 2d, "2017-01-01"));
        repository.save(new Product("Bread", 3d, "2017-01-03"));
        repository.save(new Product("Bananas", 5d, "2017-01-01"));
        repository.save(new Product("Bananas", 4d, "2017-01-02"));
        repository.save(new Product("Milk", 4d, "2017-01-04"));
    }
}
