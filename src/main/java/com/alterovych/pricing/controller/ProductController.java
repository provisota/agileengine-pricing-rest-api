package com.alterovych.pricing.controller;

import com.alterovych.pricing.model.Product;
import com.alterovych.pricing.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> listAllProducts() {
        List<Product> products = productService.findAll();
        if (products.isEmpty()) {
            return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/history/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> listAllProductsByName(@PathVariable("name") String name) {
        List<Product> products = productService.findByName(name);
        if (products.isEmpty()) {
            return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/date/{date}", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getRecentPriceReport(@PathVariable("date") String date) {
        List<Product> products = productService.findByDate(date);
        if (products.isEmpty()) {
            return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> findProduct(@PathVariable("id") long id) {
        Product product = productService.findOne(id);
        if (product == null) {
            LOGGER.info("Product with id {} not found", id);
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Void> addProduct(@RequestBody Product product, UriComponentsBuilder ucBuilder) {
        if (product.getName() == null || product.getDate() == null || product.getPrice() == null) {
            LOGGER.info("Name, price or date in the product {} couldn't be null", product);
            return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
        }
        productService.save(product);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/product/{id}").buildAndExpand(product.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Product> updateProduct(@PathVariable("id") long id, @RequestBody Product product) {
        LOGGER.info("Updating Product with id {}", id);
        Product currentProduct = productService.findOne(id);
        if (currentProduct == null) {
            LOGGER.info("Product with id {} not found", id);
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        }
        if (product.getName() == null || product.getDate() == null || product.getPrice() == null) {
            LOGGER.info("Name, price or date in the product {} couldn't be null", product);
            return new ResponseEntity<Product>(product, HttpStatus.NOT_MODIFIED);
        }
        currentProduct.setName(product.getName());
        currentProduct.setPrice(product.getPrice());
        currentProduct.setTimestamp(product.getTimestamp());
        currentProduct.setDate(product.getDate());
        productService.save(currentProduct);
        return new ResponseEntity<Product>(currentProduct, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Product> deleteProduct(@PathVariable("id") long id) {
        Product product = productService.findOne(id);
        if (product == null) {
            LOGGER.info("Unable to delete. Product with id {} not found", id);
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        }
        productService.delete(id);
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }
}
