package com.alterovych.pricing.controller;

import com.alterovych.pricing.config.ApplicationInitializer;
import com.alterovych.pricing.config.MVCConfig;
import com.alterovych.pricing.config.PersistenceJPAConfig;
import com.alterovych.pricing.model.Product;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationInitializer.class, PersistenceJPAConfig.class, MVCConfig.class},
    loader = AnnotationConfigWebContextLoader.class)
@WebAppConfiguration
public class ProductControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Test
    public void testListAllProducts()
        throws Exception {
        this.mockMvc.perform(get("/product")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$", hasSize(5)))
                    .andExpect(jsonPath("$.[0].id",is(1)))
                    .andExpect(jsonPath("$.[0].name").value("Bread"))
                    .andExpect(jsonPath("$.[0].price").value("2.0"))
                    .andExpect(jsonPath("$.[0].timestamp").value(1483228800000L))
                    .andExpect(jsonPath("$.[0].date").value("2017-01-01T02:00:00Z"))
                    .andExpect(jsonPath("$.[1].id",is(2)))
                    .andExpect(jsonPath("$.[1].name").value("Bread"))
                    .andExpect(jsonPath("$.[1].price").value("3.0"))
                    .andExpect(jsonPath("$.[1].timestamp").value(1483401600000L))
                    .andExpect(jsonPath("$.[1].date").value("2017-01-03T02:00:00Z"))
                    .andExpect(jsonPath("$.[2].id",is(3)))
                    .andExpect(jsonPath("$.[2].name").value("Bananas"))
                    .andExpect(jsonPath("$.[2].price").value("5.0"))
                    .andExpect(jsonPath("$.[2].timestamp").value(1483228800000L))
                    .andExpect(jsonPath("$.[2].date").value("2017-01-01T02:00:00Z"));
    }

    @Test
    public void testGetProduct()
        throws Exception {
        this.mockMvc.perform(get("/product/1")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id",is(1)))
                    .andExpect(jsonPath("$.name").value("Bread"))
                    .andExpect(jsonPath("$.price").value("2.0"))
                    .andExpect(jsonPath("$.timestamp").value(1483228800000L))
                    .andExpect(jsonPath("$.date").value("2017-01-01T02:00:00Z"));
    }

    @Test
    public void testFindByName()
        throws Exception {
        this.mockMvc.perform(get("/product/history/Bananas")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$.[0].id",is(3)))
                    .andExpect(jsonPath("$.[0].name").value("Bananas"))
                    .andExpect(jsonPath("$.[0].price").value("5.0"))
                    .andExpect(jsonPath("$.[0].timestamp").value(1483228800000L))
                    .andExpect(jsonPath("$.[0].date").value("2017-01-01T02:00:00Z"))
                    .andExpect(jsonPath("$.[1].id",is(4)))
                    .andExpect(jsonPath("$.[1].name").value("Bananas"))
                    .andExpect(jsonPath("$.[1].price").value("4.0"))
                    .andExpect(jsonPath("$.[1].timestamp").value(1483315200000L))
                    .andExpect(jsonPath("$.[1].date").value("2017-01-02T02:00:00Z"));
    }

    @Test
    public void testGetRecentPriceReport()
        throws Exception {
        this.mockMvc.perform(get("/product/date/2017-01-02")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$", hasSize(2)))
                    .andExpect(jsonPath("$.[0].id",is(4)))
                    .andExpect(jsonPath("$.[0].name").value("Bananas"))
                    .andExpect(jsonPath("$.[0].price").value("4.0"))
                    .andExpect(jsonPath("$.[0].timestamp").value(1483315200000L))
                    .andExpect(jsonPath("$.[0].date").value("2017-01-02T02:00:00Z"))
                    .andExpect(jsonPath("$.[1].id",is(1)))
                    .andExpect(jsonPath("$.[1].name").value("Bread"))
                    .andExpect(jsonPath("$.[1].price").value("2.0"))
                    .andExpect(jsonPath("$.[1].timestamp").value(1483228800000L))
                    .andExpect(jsonPath("$.[1].date").value("2017-01-01T02:00:00Z"));
    }

    @Ignore
    @Test
    public void testCreateProduct()
        throws Exception {
        Product product = new Product("TestProduct", 11111d);
        Gson gson = new Gson();
        String json = gson.toJson(product);

        this.mockMvc.perform(post("/product").contentType(MediaType.APPLICATION_JSON).content(json))
                    .andExpect(status().isCreated());
        this.mockMvc.perform(get("/product/11")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.name").value("TestName"))
                    .andExpect(jsonPath("$.price").value("11111.0"));
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).dispatchOptions(true).build();
    }
}