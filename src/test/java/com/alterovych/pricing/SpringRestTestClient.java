package com.alterovych.pricing;

import com.alterovych.pricing.model.Product;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * SpringRestTestClient
 * <p>
 * Test Client for testing REST API on real running API server
 */
public class SpringRestTestClient {
    public static final String REST_SERVICE_URI = "http://localhost:8080/pricing";

    /* GET */
    private static void getProduct() {
        System.out.println("Testing getProduct API----------");
        RestTemplate restTemplate = new RestTemplate();
        Product product = restTemplate.getForObject(REST_SERVICE_URI + "/product/1", Product.class);
        System.out.println(product);
    }

    /* POST */
    private static void addProduct() {
        System.out.println("Testing create Product API----------");
        RestTemplate restTemplate = new RestTemplate();
        Product product = new Product("testProduct", 11111d);
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI + "/product/", product, Product.class);
        System.out.println("Location : " + uri.toASCIIString());
    }

    /* PUT */
    private static void updateProduct() {
        System.out.println("Testing update Product API----------");
        RestTemplate restTemplate = new RestTemplate();
        Product product = new Product("updatedBananas", 55555d);
        product.setId(2L);
        restTemplate.put(REST_SERVICE_URI + "/product/2", product);
        System.out.println(product);
    }

    /* DELETE */
    private static void deleteProduct() {
        System.out.println("Testing delete Product API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI + "/product/6");
    }

    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllProducts() {
        System.out.println("Testing listAllProducts API-----------");
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> productsMap = restTemplate.getForObject(REST_SERVICE_URI + "/product/", List.class);
        if (productsMap != null) {
            for (LinkedHashMap<String, Object> map : productsMap) {
                System.out.println(
                    "Products : id=" + map.get("id") + ", Name=" + map.get("name") + ", Price=" + map.get("price") + ", Date=" + map.get("date"));
            }
        }
        else {
            System.out.println("No product exist----------");
        }
    }

    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllProductsByName(String name) {
        System.out.println("Testing listAllProductsByName API-----------");
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> productsMap = restTemplate.getForObject(REST_SERVICE_URI + "/product/history/" + name, List.class);
        if (productsMap != null) {
            for (LinkedHashMap<String, Object> map : productsMap) {
                System.out.println(
                    "Products : id=" + map.get("id") + ", Name=" + map.get("name") + ", Price=" + map.get("price") + ", Date=" + map.get("date"));
            }
        }
        else {
            System.out.println("No product exist----------");
        }
    }

    /* GET */
    @SuppressWarnings("unchecked")
    private static void getRecentPriceReport(String date) {
        System.out.println("Testing listAllProductsByDate API-----------");
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> productsMap = restTemplate.getForObject(REST_SERVICE_URI + "/product/date/" + date, List.class);
        if (productsMap != null) {
            for (LinkedHashMap<String, Object> map : productsMap) {
                System.out.println(
                    "Products : id=" + map.get("id") + ", Name=" + map.get("name") + ", Price=" + map.get("price") + ", Date=" + map.get("date"));
            }
        }
        else {
            System.out.println("No product exist----------");
        }
    }

    public static void main(String args[]) {
        listAllProducts();
        getProduct();
        addProduct();
        listAllProducts();
        updateProduct();
        listAllProducts();
        deleteProduct();
        listAllProducts();
        listAllProductsByName("Bread");
        getRecentPriceReport("2017-01-02T00:00:00Z");
    }
}