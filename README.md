# Pricing REST API for AgileEngine (test task)#

### REST API request URL's list ###

(Defined in the ProductController class)

* get list of all products: **GET */product* **
* get one product by id: **GET */product/{id}* **
* get all products by name: **GET */product/history/{name}* **
* get most recent products before given date: **GET */product/date/{date}* **
* add product: **POST */product* **
* update product: **PUT */product* **
* delete one product by id: **DELETE: */product/{id}* **